export const environment = {
  production: true,
  BACKEND_DOMAIN: 'localhost',
  BACKEND_PORT: '8001'
};
